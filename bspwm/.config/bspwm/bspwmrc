#!/bin/zsh
# Behavior
bspc config focus_follows_pointer  true
bspc config pointer_follows_monitor true
bspc config external_rules_command "$HOME/.config/bspwm/rules.sh"
bspc config window_gap     30

bspc monitor eDP1 -d 1 2 3 4
monitors=$(bspc query -M --names)
if [ $(echo $monitors | wc -l) -gt 1 ]; then
    i=5;j=6
    for monitor_name in $(echo $monitors | grep -v eDP1); do
        bspc monitor $monitor_name -d $i $j
        i=$((i+2))
        j=$((j+2))
    done
fi

# Window decoration
bspc config border_width 0
bspc config focused_border_color  "#928374"
bspc config normal_border_color   "#504945"
bspc config presel_feedback_color "#83A598"


# Rules
bspc rule -r '*'
bspc rule -a Screenkey       manage=off
bspc rule -a Pavucontrol     state=floating
bspc rule -a Blueman-manager state=floating
bspc rule -a Pcmanfm         state=floating
bspc rule -a Zathura         state=tiled
bspc rule -a Civ5XP          state=fullscreen
bspc rule -a guvcview        state=floating
bspc rule -a mpv             state=floating sticky=on rectangle=960x540+1575+275

# Programs
run() {
    if [ "$1" = "-f" ]; then
        cmd="${@:2}"
        pgrep -f "$cmd" || (${@:2} &)
    else
        pgrep "$1" || ($@ &)
    fi
}

# Bindings
run sxhkd -m -1
run xcape -e 'Control_L=Escape'

# Pointer behavior
xsetroot -cursor_name left_ptr
run unclutter

# Type speed/delay
xset r rate 300 50 &

# Screen locker
run xss-lock -- physlock

# Automount disks
run udiskie

# Wallpaper
feh --bg-fill ~/pic/wallpapers/chicken-wall.png
# Random wallpaper
# feh --bg-fill $(find ~/pic/wallpapers -type f ! -path '*originals*' \
#                     | shuf -n1 --random-source=/dev/urandom)

# Clipboard
run -f autocutsel -selection CLIPBOARD -fork
run -f autocutsel -selection PRIMARY -fork

# Multi-monitor polybar setup - new polybar instance for each monitor
killall -q polybar
while pgrep -u $UID -x polybar >/dev/null; do sleep 1; done
for m in $(bspc query -M --names); do
    MONITOR=$m polybar simple &
done
ln -sf /tmp/polybar_mqueue.$! /tmp/ipc-polybar-simple

# Floating st + tmux (see rules.sh)
if [ -z "$(pgrep -f st-float)" ]; then
    if [ -z "$(pgrep tmux)" ]; then
        st -n st-float -e sh -c 'TERM=screen-256color tmux' &
    else
        st -n st-float -e sh -c 'TERM=screen-256color tmux attach' &
    fi
fi

# Run emacs in background, connect with emacsclient
run emacs --daemon

compton -b
