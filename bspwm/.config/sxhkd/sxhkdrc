# -*- mode: conf -*-
#
# wm independent hotkeys
#

# terminal emulator
super + Return
    id=$(</tmp/mpv-float) && \
    bspc node $id --flag hidden && \
    bspc node -f $id

super + w
    rofi -show window

super + d
    rofi -show drun -show-icons

super + z
    id=$(</tmp/st-float) && \
    bspc node $id --flag hidden && \
    bspc node -f $id
# program launcher

alt + space
    rofi -show run

# passwords
super + p
    passmenu -p "pass"

# make sxhkd reload its configuration files:
super + Escape
    pkill -USR1 -x sxhkd

#
# bspwm hotkeys
#

# quit bspwm normally
super + shift + period
    wm-exit-dmenu

super + shift + p
   ~/.config/bspwm/bspwmrc

# close and kill
super + {_,shift + }apostrophe
    bspc node -{c,k}

# alternate between the tiled and monocle layout
super + m
    bspc desktop -l next

# if the current node is automatic, send it to the last manual, otherwise pull
# the last leaf
super + y
    bspc query -N -n focused.automatic && \
    bspc node -n last.!automatic || bspc node last.leaf -n focused

# swap the current node and the biggest node
super + g
    bspc node -s biggest.local

# rotate
super + r
    bspc node @/ -R 90

super + R
    bspc node @/ -R -90
#
# state/flags
#

# set the window state
super + {period,u,f}
    bspc node -t {'~tiled','~fullscreen','~floating'}

# set the node flags
super + ctrl + {x,y,z}
    bspc node -g {locked,sticky,private}

ctrl + alt + Delete
    physlock


# focus/swap
#

# focus the node in the given direction
super + {_,shift + }{h,t,n,s}
    bspc node -{f,s} {west,south,north,east}

# focus the node for the given path jump
# super + {p,b,comma,period}
#     bspc node -f @{parent,brother,first,second}

# focus the next/previous node in the current desktop
super + {_,shift + }c
    bspc node -f {next,prev}.local

# focus the next/previous desktop in the current monitor
super + bracket{left,right}
    bspc desktop -f {prev,next}.local

# focus the last node/desktop
super + {grave,Tab}
    bspc {node,desktop} -f last

# focus the older or newer node in the focus history
super + {o,i}
    bspc wm -h off; \
    bspc node {older,newer} -f; \
    bspc wm -h on

# focus or send to the given desktop
super + {_,shift + }{1-9,0}
    bspc {desktop -f,node -d} '^{1-9,10}'

#
# preselect
#

# preselect the direction
super + ctrl + {h,t,n,s}
    bspc node -p {west,south,north,east}

# preselect the ratio
super + ctrl + {1-9}
    bspc node -o 0.{1-9}

# move into preselection
super + l
    bspc node -n last.!automatic.local

# cancel the preselection for the focused node
super + ctrl + space
    bspc node -p cancel

# cancel the preselection for the focused desktop
super + ctrl + shift + space
    bspc query -N -d | xargs -I id -n 1 bspc node id -p cancel

#
# move/resize
#

# expand a window by moving one of its side outward
super + alt + {h,t,n,s}
    bspc node -z {left -20 0,bottom 0 20,top 0 -20,right 20 0}

# contract a window by moving one of its side inward
super + alt + shift + {h,t,n,s}
    bspc node -z {right -20 0,top 0 20,bottom 0 -20,left 20 0}

# move a floating window
super + {Left,Down,Up,Right}
    bspc node -v {-20 0,0 20,0 -20,20 0}

XF86MonBrightnessUp
    brightness=$(printf "%0.f" `xbacklight`) \
    && [[ $brightness -lt 5 ]] \
    && xbacklight -set $(($brightness + 1)) \
    || xbacklight -set $(($brightness + 5))
XF86MonBrightnessDown
    brightness=$(printf "%0.f" `xbacklight`) \
    && [[ $brightness -le 5 ]] \
    && xbacklight -set $(($brightness - 1)) \
    || xbacklight -set $(($brightness - 5))
# XF86AudioPrev
#     playerctl previous
# XF86AudioNext
#     playerctl next
# XF86AudioPlay
#     playerctl play-pause
super + F1
    playerctl play-pause
super + F2
    playerctl previous
super + F3
    playerctl next
XF86AudioMute
    amixer -D pulse sset Master toggle
XF86AudioLowerVolume
    amixer -D pulse sset Master 5%-
XF86AudioRaiseVolume
    amixer -D pulse sset Master 5%+
XF86AudioMicMute
    amixer set Capture toggle
XF86Display
    scrot ~/pic/scrot/%Y-%m-%d_%H:%M:%S.png && notify-send "scrot" "Screenshot saved to ~/pic/scrot"
shift + XF86Display
    scrot -s ~/pic/scrot/%Y-%m-%d_%H:%M:%S.png && notify-send "scrot" "Window screenshot saved to ~/pic/scrot"
